{:installed {:awesome {:distros {:arch {:name "awesome-git"}} :name "awesome"}
             :emacs {:distros {:arch {:name "emacs"}} :name "emacs"}
             :fennel {:distros {:arch {:name "fennel"}
                                :luarocks {:name "fennel"}}
                      :name "fennel"}
             :hello {:distros {:arch {:name "hello"}} :name "hello"}
             :htop {:distros {:arch {:name "htop-vim"}} :name "htop"}
             :lua {:distros {:arch {:name "lua"}} :name "lua"}
             :luarocks {:distros {:arch {:name "lua"}} :name "luarocks"}
             :tym {:distros {:arch {:name "tym-git"}} :name "tym"}
             :vivaldi {:distros {:arch {:name "vivaldi"}} :name "vivaldi"}
             :wezterm {:distros {:arch {:name "wezterm-git"}} :name "wezterm"}
             :xplr {:distros {:arch {:name "xplr"}} :name "xplr"}}
 :uninstalled {}}