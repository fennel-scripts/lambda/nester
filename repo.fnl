#!/usr/bin/env fennel

(local package (require :lambda.package))

(local hello-data
	   {:name "hello"
		:tags ["example" "hello" "hello world"]
		:desc ["Prints Hello World and more"
			   "GNU hello, the standard gnu hello-world"]
		:description
		[(table.concat ["unlike the minimal version often seen," "GNU Hello processes its argument list to modify its behavior," "supports greetings in many languages, and so on"] "\n\t")]
		:distros {:arch {:name "hello"}}})
(local hello (package.new hello-data))

(local vifm-data
		{:name "vifm"
		 :tags ["file-manager" "tui" "terminal-app"]
		 :distros {:arch {:name "vifm"}}})

(local vifm (package.new vifm-data))
(local vifm-git (package.new vifm-data))
(set vifm-git.distros.arch.name "vifm-git")


(local htop-data
	   {:name "htop"
		:tags ["process viewer" "tui" "terminal-app"]
		:desc ["Interactive process viewer"
			   "a better system monitor"]
		:distros {:arch {:name "htop"}}})

(local htop (package.new htop-data))
(local htop-git (package.new htop-data))
(local htop-vim (package.new htop-data))
(local htop-minimal (package.new htop-data))
(local htop-solarized (package.new htop-data))

(local htop-vim-git (package.new htop-data))
(local htop-vim-solarized-git (package.new htop-data))

(set htop-git.distros {:arch {:name "htop-git"}})
(set htop-vim.distros {:arch {:name "htop-vim"}})
(set htop-solarized.distros {:arch {:name "htop-solarized"}})
(set htop-vim-git.distros {:arch {:name "htop-vim-git"}})
(set htop-vim-solarized-git.distros {:arch {:name "htop-vim-solarized-git"}})
(set htop-minimal.distros {:arch {:name "htop-minimal"}})

(local lolcat-data
	   {:name "lolcat"
		:tags ["lua" "fennel"]
		:desc ["Terminal string styling. Put some color in your console!"]
		:description ["Terminal string styling."
					  "Put some color in your console! Support for Fennel, Lua, and Shell"]
		:distros {:arch {:name "lolcat"}}})

(local lolcat (package.new lolcat-data))
(local lolcat-c (package.new lolcat-data))
(set lolcat-c.distros {:arch {:name "c-lolcat"}})

(local supernova-data
	   {:name "supernova"
		:tags ["lua" "fennel"]
		:desc ["Terminal string styling. Put some color in your console!"]
		:description ["Terminal string styling."
					  "Put some color in your console! Support for Fennel, Lua, and Shell"]
		:distros {:luarocks {:name "supernova"}}})
(local supernova (package.new supernova-data))

(local blahaj-data
	   {:name "blahaj"
		:tags ["crystal-lang" "lgbtqia+" "one-topic" "one-topic-at-a-time" "CLI-tool"]
		:desc ["Gay sharks at your local terminal"
			   "lolcat-like CLI tool"]
		:distros {:arch {:name "blahaj"}}})
(local blahaj		(package.new blahaj-data))
(local blahaj-bin	(package.new blahaj-data))
(local blahaj-git	(package.new blahaj-data))
(set blahaj-git.distros {:arch {:name "blahaj-git"}})
(set blahaj-bin.distros {:arch {:name "blahaj-bin"}})

(local queercat-data
	   {:name "queercat"
		:tags ["lgbtqia+" "CLI-tool"]
		:desc ["A version of lolcat with options for some lgbtq+ flags"]
		:distros {:arch {:name "queercat-git"}}})
(local queercat (package.new queercat-data))

(local dotacat-data
	   {:name "dotacat"
		:tags ["rust" "fast" "CLI-tool"]
		:desc ["like lolcat, but fast"]
		:distros
		{:arch {:name "dotacat"}
		 :cargo {:name "dota2cat"}}})
(local dotacat (package.new dotacat-data))


(local lua-data
	   {:name "lua"
		:tags ["lua" "programming language"]
		:desc ["Powerful lightweight programming language designed for extending applications "]
		:distros {:arch {:name "lua"}}})

(local lua* (package.new lua-data))

(local luarocks-data
	   {:name "luarocks"
		:tags ["lua" "package manager"]
		:desc ["Deployment and management system for Lua modules"]
		:distros {:arch {:name "lua"}}})

(local luarocks (package.new luarocks-data))

(local fennel-data
	   {:name "fennel"
		:tags ["lua" "lisp" "programming language"]
		:desc ["A lua Lisp language" "lisp that compiles to lua"]
		:distros {:arch {:name "fennel"} :luarocks {:name "fennel"}}} )

(local fennel (package.new fennel-data))
(local fennel-git (package.new fennel-data))
(set fennel-git.distros {:arch {:name "fennel-git"}})

(local awesome-data
	   {:name "awesome"
		:tags ["window-manager" "lua" "tilling" "powerful" "fast" ]
		:desc ["Highly configurable framework window manager"]
		:distros {:arch {:name "awesome"}}})

(local awesome (package.new awesome-data))
(local awesome-git (package.new awesome-data))
(set awesome-git.distros {:arch {:name "awesome-git"}})



(local xplr-data
	   {:name "xplr"
		:tags ["file-explorer" "tui" "terminal-app"
			   "rust" "lua"]
		:desc ["A hackable, minimal, fast TUI file explorer"]
		:distros {:arch {:name "xplr"}}})

(local xplr (package.new xplr-data))
(local xplr-git (package.new xplr-data))
(local xplr-bin (package.new xplr-data))

(set xplr-git.distros {:arch {:name "xplr-git"}})
(set xplr-bin.distros {:arch {:name "xplr-bin"}})


(local tym-data
	   {:name "tym"
		:tags ["terminal-emulator" "lua" "light-weight" "powerful" "fast" "service-client" "minimal"]
		:desc ["Lua-configurable terminal emulator"]
		:distros {:arch {:name "tym"}}})

(local tym (package.new tym-data))
(local tym-git (package.new tym-data))
(set tym-git.distros {:arch {:name "tym-git"}})

(local wezterm-data
	   {:name "wezterm"
		:tags ["terminal-emulator" "lua" "rust" "powerful" "fast" "GPU-accelerated" "service-client"]
		:desc ["A terminal emulator implemented in Rust, using OpenGL ES 2 for rendering."
			   "A GPU-accelerated cross-platform terminal emulator and multiplexer"]
		:distros {:arch {:name "wezterm"}}})
(local wezterm (package.new wezterm-data))
(local wezterm-git (package.new wezterm-data))

(set wezterm-git.distros {:arch {:name "wezterm-git"}})



(local miniterm-data {:name "miniterm"
					  :tags ["terminal-emulator" "light-weight" "fast" "minimal"]
					  :desc ["Lightweight VTE terminal emulator with colorscheme support"]
					  :distros {:arch {:name "miniterm"}}})
(local miniterm (package.new miniterm-data))
(local miniterm-git (package.new miniterm-data))
(set miniterm-git.distros {:arch {:name "miniterm-git"}})




(local emacs-data
	   {:name "emacs"
		:tags ["text-editor" "code-editor" "lisp" "emacs" "terminal" "gui"]
		:desc ["The extensible, customizable, self-documenting real-time display editor"
			   "text and code editor, sometimes called an operating-system"
			   "a fully featured operating-system, lacking only a decent text-editor"
			   "the other GNU text editor, this time written in lisp"
			   "the best code-editor for hackers"
			   "(emacs-)lisp environment with a builtin text-editor"]
		:distros {:arch {:name "emacs"}}})

(local emacs (package.new emacs-data))
(local emacs-git (package.new emacs-data))
(local emacs-nox (package.new emacs-data))
(local emacs-nativecomp (package.new emacs-data))

(set emacs-git.distros {:arch {:name "emacs-git"}})
(set emacs-nox.distros {:arch {:name "emacs-nox"}})
(set emacs-nativecomp.distros {:arch {:name "emacs-nativecomp"}})


(local neovim-data
	   {:name "neovim"
		:tags ["text-editor" "tui" "terminal-app" "lua"]
		:desc ["a fully featured text-editor, lacking only a decent operating-system, for that you will need to install emacs"
			   "ne(w)o VI IMproved"
			   "Fork of Vim aiming to improve user experience, plugins, and GUIs"
			   ]
		:distros {:arch {:name "neovim"}}})

(local neovim (package.new neovim-data))
(local neovim-git (package.new neovim-data))
(set neovim-git.distros {:arch {:name "neovim-git"}})


(local vim-data
	   {:name "vim"
		:tags ["text-editor" "tui" "terminal-app"]
		:desc ["a fully featured text-editor, lacking only a decent operating-system, for that you will need to install emacs"
			   "Vi Improved, a highly configurable, improved version of the vi text editor"]
		:distros {:arch {:name "vim"}}})
(local vim (package.new vim-data))
(local vim-git (package.new vim-data))
(local vim-minimal (package.new vim-data))

(set vim-git.distros {:arch {:name "vim-git"}})
(set vim-minimal.distros {:arch {:name "vim-minimal"}})


(local vivaldi-data
	   {:name "vivaldi"
		:tags ["web-explorer" "web browser" "email-client" "rss-feed reader" "note-taking" "calendar"]
		:desc ["An advanced browser made with the power user in mind."
			   "the browser that firefox could have become"
			   "the linux of web-browsers"
			   "the emacs of web-browsers"
			   "a better alternative to firefox"
			   "a fully featured operating-system, lacking only a decent browser-engine"]
		:distros {:arch {:name "vivaldi"}}})

(local vivaldi (package.new vivaldi-data))




{
 :hello		{: hello :default hello}
 :xplr		{: xplr :default xplr :git xplr-git :bin xplr-bin}
 :vifm		{: vifm :default vifm :git vifm-git}
 :vivaldi	{: vivaldi :default vivaldi}
 :neovim	{: neovim :nvim neovim :default neovim :git neovim-git}
 :nvim		{: neovim :nvim neovim :default neovim :git neovim-git}
 :vim		{: vim :default vim :git vim-git}
 :tym		{: tym :default tym :git tym-git}
 :miniterm	{: miniterm :default miniterm-git :git miniterm-git}
 :wezterm	{: wezterm :default wezterm :git wezterm-git}
 :luarocks	{: luarocks :default luarocks}
 :lua		{:lua lua* :default lua*}
 :fennel	{: fennel :default fennel :git fennel-git}
 :awesome	{: awesome :default awesome :git awesome-git}
 :lolcat	{: lolcat :default lolcat-c :c-lolcat lolcat-c : lolcat-c}
 :dotacat	{: dotacat :default dotacat}
 :blahaj	{: blahaj :default blahaj-bin :bin blahaj-bin :git blahaj-git}
 :queercat	{: queercat :queercat-git queercat}
 :supernova {: supernova :default supernova}
 :emacs {: emacs
         :default emacs
         :git emacs-git
         :nox emacs-nox
         :nativecomp emacs-nativecomp}
 :htop {: htop
        :default htop
        :git htop-git
        :vim htop-vim
        :solarized htop-solarized
        :vim-git htop-vim-git
        ;;:vim-solarized htop-vim-solarized
        :vim-solarized-git htop-vim-solarized-git
        :minimal htop-minimal}}

;;{:hello {: hello :default hello}}
