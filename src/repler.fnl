#!/usr/bin/env fennel
(local fennel (require :fennel))
(local package (require :lambda.package))
(local distro (require :lambda.distributions.arch))
(local utils (require :lambda.utils))
(local packman distro.package-manager)

(lambda pp [...] (print (fennel.view ...)))

(local manifestor ((. (require :lambda.manifestor) :new)) )
(manifestor:init)
(local manifest manifestor.manifest)


;;(pp manifest)
(packman:init manifest)




(fennel.repl {:env {:packman packman : require : print  }})

(manifestor:write)
