#!/usr/bin/env fennel
;;(pp manifest)
;;(packman.update)
;;(tset manifest :date (os.time));;update timestamp
(when false
  (packman:install repo.lua.default)
  (packman:install repo.luarocks.default)
  (packman:install repo.fennel.default)
  (packman:install repo.xplr.default)
  (packman:install repo.tym.git)
  (packman:install repo.awesome.git)
  (packman:install repo.emacs.default)
  (packman:install repo.wezterm.git)
  (packman:install repo.htop.vim)
  (packman:install repo.vivaldi.default)
  (packman:install repo.hello.default)
)
;;(packman:info repo.emacs.default)

;;(packman:install repo.hello :default)

;;(packman:install {:default manifest.installed.hello} :default)

(when false
  (each [name pkg (pairs manifest.installed)]
	(print (.. "operating on " name))
	(packman:install {:default pkg})
	;;(pp pkg)
	)


  ;;(packman:install (. (require :repo.repo-meta) :yash))
  (packman:uninstall repo.hello)
  (packman:install repo.hello)
  )

;;(pp text-edit-meta.vim)
;;(packman:install terminal-meta.tym :git)
;;(packman:install terminal-meta.wezterm :git)
;;(packman:meta lolcat-meta.dotacat)
;;(packman:meta lolcat-meta.supernova)
;;(pacman:install lolcat-meta.blahaj.default)
