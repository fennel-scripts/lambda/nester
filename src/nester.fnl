#!/usr/bin/env fennel
(local fennel (require :fennel))
;;(local package (require :lambda.package))
(local distro (require :lambda.distributions.dummy))
(local utils (require :lambda.utils))
(local packman distro.package-manager)
(local repo (require :repo))

(local manifestor ((. (require :lambda.manifestor) :new)))

(lambda pp [...] (print (fennel.view ...)))

(lambda setup []
  (manifestor:init)
  (local manifest manifestor.manifest)
  (packman:init manifest)
  )



;;(lambda update [] )
;;(lambda refresh [] )

(lambda list-repo []
  (each [key value (pairs repo)]
	  (tset value :default nil)
	(each [k _v (pairs value)]
      (print key k))))





(lambda finish []
  ;;(pp manifest)
  (manifestor:write)
  (utils.tprint ["operations" "took" "about" (os.clock) "seconds"] " ")
  )


(lambda menu [args]
  (lambda list-menu [args]
	(pp args)
	(case (table.remove args 1)
	  :repo (list-repo)
	  )
	)
  
	(pp args)
  (case (table.remove args 1)
	:list (list-menu args)
	)
  
  
  
  )





(setup)
;;(list-repo)
(menu arg)
(finish)
