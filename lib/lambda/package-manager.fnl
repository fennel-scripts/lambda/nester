#!/usr/bin/env fennel
;;(local package (require :lambda.package))
(local utils (require :lambda.utils))
(local tcat table.concat)

(local manager {})


;;(local default {})


(lambda view [this pkg]
  (print
   (tcat
	[(tcat ["Name:" pkg.name] " ")
	 (tcat ["Tags:" (tcat pkg.tags ", ")] " ")
	 (or (tcat ["Desc:" (. pkg.desc (math.random (# pkg.desc)))] " ") " ")
	 ;;(when pkg.description (tcat ["Desc:" (. pkg.description (math.random 1 (# pkg.description)))]))
	 ] "\n")) 
  )


(lambda manager.new [{: name : install : uninstall : update : init :info ?info}]
  {: name
   : install
   :add install
   :push install
   : uninstall
   :remove uninstall
   :pop uninstall
   : update
   :manifest {}
   : init
   :setup init
   :info (or ?info view)
   :information (or ?info view)
   }
  )





manager
