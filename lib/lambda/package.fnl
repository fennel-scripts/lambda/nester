#!/usr/bin/env fennel
(local package {})
(local fennel (require :fennel))
(lambda package.new [{: name :tags ?tags :distros ?distros :desc ?desc :description ?description}]
  (local pkg
		 {:name name :distros (or ?distros {:default name})
		  :tags (or ?tags [:none])
		  :desc (or ?desc ?description [:none])
		  :description (or ?description ?desc [:none])
		  })
  
  (fn view [tbl view options indent]

	(view {:name tbl.name :distros tbl.distros})
	
	)
	 
  (setmetatable pkg {:__fennelview view})

  )



package
