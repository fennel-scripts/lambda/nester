#!/usr/bin/env fennel
(local package (require :lambda.package))
(local distro (require :lambda.distro))
(local package-manager (require :lambda.package-manager))
(local utils (require :lambda.utils))
(local tcat table.concat)
(local dummy {:name :dummy :cmd "somecmd"})

(lambda dummy.init [this manifest]
  (tset this :manifest manifest))

(local exec (fn [...] (print (tcat ...))))

(lambda dummy.install [this pkg]
  (exec [dummy.cmd :-S :--needed (or (?. pkg :distros :dummy :name) pkg.name)] " ")
  (tset this.manifest :installed pkg.name pkg)
  (tset this.manifest :uninstalled pkg.name nil))

(lambda dummy.uninstall [this pkg]
  (exec [dummy.cmd :-R (or (?. pkg :distros :dummy :name) pkg.name)] " ")
  (tset this.manifest :installed pkg.name nil)
  (tset this.manifest :uninstalled pkg.name pkg))

(lambda dummy.update []
  (exec [dummy.cmd :update] " ")
  (exec [dummy.cmd :upgrade] " ")
  )


(local pkgmngr (package-manager.new dummy))
(distro.new
 {:name :dummy
  :package-manager dummy
  :package-managers {: dummy}})

