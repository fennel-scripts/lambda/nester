#!/usr/bin/env fennel
(local package (require :lambda.package))
(local distro (require :lambda.distro))
(local package-manager (require :lambda.package-manager))
(local utils (require :lambda.utils))
(lambda pp [...] (let [fennel (require :fennel)] (print (fennel.view ...))))
(local tcat utils.tcat)
(local pm {:name :pacman :cmd "sudo pacman"})



(lambda pm.init [this manifest]
  (tset this :manifest manifest))

(local exec utils.exec)

(lambda meta [pkg ?args]
  (if ?args
	  (. pkg ?args)
	  (. pkg :default)))

(lambda x [pkg ?args] (pp (meta pkg ?args)))

(lambda pm.install [this pakg ?args]
  (let [pkg (meta pakg ?args)]
	(exec [pm.cmd :-S :--needed (or (?. pkg :distros :arch :name) pkg.name)] " ")
	(tset this.manifest :installed pkg.name pkg)
	(tset this.manifest :uninstalled pkg.name nil)))

(lambda pm.uninstall [this pakg ?args]
  (let [pkg (meta pakg ?args)]
	(exec [pm.cmd :-R (or (?. pkg :distros :arch :name) pkg.name)] " ")
	(tset this.manifest :installed pkg.name nil)
	(tset this.manifest :uninstalled pkg.name pkg)))

(lambda pm.update []
  (exec [pm.cmd :-Syyu :--noconfirm :--sudoloop] " "))

(local parux {:name :paru :cmd :paru})
(lambda parux.init [this manifest]
  (tset this :manifest manifest))

(lambda parux.install [this pakg ?args]
  (pp pakg (meta pakg ?args))
  (let [pkg (meta pakg ?args)]
	(exec [parux.cmd :-S :--needed (or (?. pkg :distros :arch :name) pkg.name)] " ")
	(tset this.manifest :installed pkg.name pkg)
	(tset this.manifest :uninstalled pkg.name nil)))

(lambda parux.uninstall [this pakg ?args]
  (let [pkg (meta pakg ?args)]
	(exec [parux.cmd :-R (or (?. pkg :distros :arch :name) pkg.name)] " ")
	(tset this.manifest :uninstalled pkg.name pkg)
	(tset this.manifest :installed pkg.name nil)))

(lambda parux.update []
  (exec [parux.cmd :-Syyu :--noconfirm :--sudoloop] " "))

(local pacman (package-manager.new pm))
(local paru (package-manager.new parux))

(lambda paru.meta [_ pkg ?args]
  (x pkg ?args))

(local arch
	   (distro.new
		{:name :arch
         :package-manager paru
         :package-managers {: pacman : paru}}))
arch

