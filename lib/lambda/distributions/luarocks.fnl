#!/usr/bin/env fennel
(local package (require :lambda.package))
(local distro (require :lambda.distro))
(local package-manager (require :lambda.package-manager))
(local utils (require :lambda.utils))
(local tcat utils.tcat)
(local lrocks {:name :luarocks :cmd "luarocks "})

(lambda lrocks.init [this manifest]
  (tset this :manifest manifest))

(local exec (fn [...] (print (utils.tcat ...))))

(lambda lrocks.install [this pkg]
  (exec [lrocks.cmd :install (or (?. pkg :distros :luarocks :name) pkg.name)] " ")
  (tset this.manifest :installed pkg.name pkg)
  (tset this.manifest :uninstalled pkg.name nil))

(lambda lrocks.uninstall [this pkg]
  (exec [lrocks.cmd :remove (or (?. pkg :distros :luarocks :name) pkg.name)] " ")
  (tset this.manifest :installed pkg.name nil)
  (tset this.manifest :uninstalled pkg.name pkg))

(lambda lrocks.update []
  (exec [lrocks.cmd :update] " ")
  (exec [lrocks.cmd :upgrade] " ")
  )


(local luarocks (package-manager.new lrocks))
(distro.new
 {:name :luarocks
  :package-manager luarocks
  :package-managers {: luarocks}})

