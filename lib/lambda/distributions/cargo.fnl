#!/usr/bin/env fennel
(local package (require :lambda.package))
(local distro (require :lambda.distro))
(local package-manager (require :lambda.package-manager))
(local utils (require :lambda.utils))
(local tcat utils.tcat)
(local cargo {:name :cargo :cmd "cargo"})

(lambda cargo.init [this manifest]
  (tset this :manifest manifest))

(local exec (fn [...] (print (utils.tcat ...))))

(lambda cargo.install [this pkg]
  (exec [cargo.cmd :install (or (?. pkg :distros :cargo :name) pkg.name)] " ")
  (tset this.manifest :installed pkg.name pkg)
  (tset this.manifest :uninstalled pkg.name nil))

(lambda cargo.uninstall [this pkg]
  (exec [cargo.cmd :uninstall (or (?. pkg :distros :cargo :name) pkg.name)] " ")
  (tset this.manifest :installed pkg.name nil)
  (tset this.manifest :uninstalled pkg.name pkg))

(lambda cargo.update []
  (exec [cargo.cmd :update] " ")
  )


(local pkgmngr (package-manager.new cargo))
(distro.new
 {:name :cargo
  :package-manager pkgmngr
  :package-managers {:cargo pkgmngr}})

