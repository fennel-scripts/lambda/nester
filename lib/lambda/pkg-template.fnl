#!/usr/bin/env fennel
(local template {})

(lambda template.new [{:tags ?tags :distros ?distros :desc ?desc :description ?description}]
		 {:distros (or ?distros {})
		  :tags (or ?tags [:none])
		  :desc (or ?desc ?description [])
		  :description (or ?description ?desc [])}
		 )

template
