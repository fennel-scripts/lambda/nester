#!/usr/bin/env fennel
(local manifestor {})

(lambda manifestor.read [this]
  (tset this :manifest (require :manifest))
  this
  )

(lambda manifestor.write [this]
  (let [m (io.open "manifest.fnl" :w)
		fennel (require :fennel)]
	(m:write (fennel.view this.manifest))
	(m:flush)
	(m:close)))

(lambda manifestor.init [this]
  (this:read)
  (when (not this.manifest.installed)
	(tset this :manifest :installed {}))
  (when (not this.manifest.uninstalled)
	(tset this :manifest :uninstalled {})))



(lambda manifestor.new []
  {:manifest {}
   :read manifestor.read
   :load manifestor.read
   :write manifestor.write
   :save manifestor.write
   :init manifestor.init
   :setup manifestor.init
   }
  )


manifestor
