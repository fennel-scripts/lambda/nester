#!/usr/bin/env fennel

(local distro {})

(lambda distro.new [{: name : package-manager :package-managers ?package-managers}]
  (local dist {: name : package-manager :package-managers {:default package-manager}})
  (when ?package-managers
	(each [name pcm (pairs ?package-managers)]
	  (tset dist.package-managers name pcm)))
  dist
  )
distro
